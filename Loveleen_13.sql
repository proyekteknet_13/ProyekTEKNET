create database Travelancar;

Create  table Hotel
(kode_hotel varchar(10) primary key not null,
nama_hotel varchar (10),
tipe_hotel varchar
)

Create table Kamar
(id_tipe_kamar varchar(10) primary key not null,
id_hotel int,
jumlah_kamar int,
harga_kamar int,
keterangan varchar
)

Create table Pemesanan
(hari_h date,
hari_out date,
jumlah_pesanan int,
total_harga int
)

Create table StatusInvoice
(
kode_pemesanan varchar,
status varchar,
total_biaya int,
waktu_pemesanan datetime,
perubahan_terakhir datetime)

Create table Pembatalan
(
kode_refund varchar,
waktu datetime)

Create table Penerbangan
(
no_penerbangan varchar primary key not null,
maskapai varchar,
jam char,
dari varchar, 
tujuan varchar,
jumlah_tiket int,
harga int,
hari char)

Create table Customer
(id int primary key not null,
nama_lengkap varchar,
tanggal_lahir date,
alamat varchar,
kota varchar,
provinsi varchar,
email varchar,
telepon varchar,
kata_sandi varchar
)

Create table Pembayaran
(
jenis_pembayaran varchar,
no_referensi_bank varchar,
nama_pengirim varchar,
nama_bank_pengirim varchar,
nominal_terbayar varchar,
waktu datetime,
perubahan_terakhir datetime)

Create table BankKonservatif
(
kode_bank varchar primary key not null,
nama_bank varchar)

Create table BankBung(
kode_bank varchar primary key not null,
nama_bank varchar)